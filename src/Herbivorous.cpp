#include <fstream>

#include "Herbivorous.h"

Herbivorous::Herbivorous(char* name, float w, int h, char* stat, bool home, int num_eyes) : Pet(name, w, h), location_home(home), number_of_eyes(num_eyes)
{
    classname = HERBIVOROUS;
    std::strcpy(status, stat);
}
void Herbivorous::print(std::ostream& ostream) const
{
    Pet::print(ostream);
    ostream << "C��������: " << status << std::endl;
    ostream << "�������� ��������: " << location_home << std::endl;
    ostream << "���������� ����: " << number_of_eyes << std::endl;
}

void Herbivorous::save(std::ofstream& fout) {
    Pet::save(fout);
    fout.write(reinterpret_cast<char*>(status), sizeof(status));
    fout.write(reinterpret_cast<char*>(&location_home), sizeof(location_home));
    fout.write(reinterpret_cast<char*>(&number_of_eyes), sizeof(number_of_eyes));
}


void Herbivorous::load(std::ifstream& fin) {
    Pet::load(fin);
    fin.read(reinterpret_cast<char*>(status), sizeof(status));
    fin.read(reinterpret_cast<char*>(&location_home), sizeof(location_home));
    fin.read(reinterpret_cast<char*>(&number_of_eyes), sizeof(number_of_eyes));
}
