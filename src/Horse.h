#pragma once


#include "Herbivorous.h"

class Horse : public Herbivorous
{
protected:
    float speed;
public:
        Horse(char*, float, int, char*, bool, int, float);
        Horse();
        void print(std::ostream&) const;
        void save(std::ofstream&);
       void load(std::ifstream&);

};