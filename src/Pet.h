#pragma once

#include <iostream>
#include <string>

#include "config.h"

class Pet
{
protected:
    ClassName classname = PET;
    char animal_name[STRING_SIZE];
    float weight;
    int height;
public:
    Pet();
    Pet(char*, float, int);
    char* getName();
    float getWeight();
    int getHeight();
    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    friend std::ostream& operator<<(std::ostream&, const Pet&);

};
