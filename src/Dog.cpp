#include <fstream>

#include "Dog.h"

Dog::Dog(
    char* name,
    float w,
    int h,
    char* message,
    bool nv,
    int num_paws,
    char* host,
    float distance,
    int num_command
) : Predator(name, w, h, message, nv, num_paws), distance_traveled(distance), number_of_command(num_command)
{
    classname = DOG;
    std::strcpy(host_name, host);
    
}
Dog::Dog() : Predator("qq", 42, 5, "prr", true, 4), distance_traveled(7.5), number_of_command(99)
{
    classname = DOG;
    std::strcpy(host_name, "VOVCHIKKK");

}
void Dog::print(std::ostream& ostream) const
{
    Predator::print(ostream);
    ostream << "��� �������: " << host_name << std::endl;
    ostream << "���������� ���������: " << distance_traveled << std::endl;
    ostream << "���������� ��������� ������: " << number_of_command << std::endl;
}

void Dog::save(std::ofstream& fout) {
    Predator::save(fout);
    fout.write(reinterpret_cast<char*>(host_name), sizeof(host_name));
    fout.write(reinterpret_cast<char*>(&distance_traveled), sizeof(distance_traveled));
    fout.write(reinterpret_cast<char*>(&number_of_command), sizeof(number_of_command));
}


void Dog::load(std::ifstream& fin) {
    Predator::load(fin);
    fin.read(reinterpret_cast<char*>(host_name), sizeof(host_name));
    fin.read(reinterpret_cast<char*>(&distance_traveled), sizeof(distance_traveled));
    fin.read(reinterpret_cast<char*>(&number_of_command), sizeof(number_of_command));
}
