#include <iostream>
#include <fstream>
#include <cstring>
#include <map>

#include <Windows.h>
#include <ctime>

#include "Pet.h"
#include "Cat.h"
#include "Cow.h"
#include "Dog.h"
#include "Horse.h"
#include "menu.h"
#include "config.h"
#include "readfile.h"
#include "List.h"



class CStringComparator { 
public:
    bool operator()(const char* first, const char* second) const {
        return std::strcmp(first, second) < 0;
    }
};


using CmdMap = std::map<const char*, void (*)(const char*), CStringComparator>;


List<Pet> list;


void menu(const char* line) { 
    setlocale(LC_ALL, "Russian");
    std::cout
        << "1. menu - ��������� �������" << std::endl 
        << "2. load '��� �����' - ��������� ������ �� �����" << std::endl 
        << "3. save '��� �����' - ��������� ������ � ����" << std::endl 
        << "4. print - ������� ������ �� �����" << std::endl 
        << "5. add - �������� ����� ������" << std::endl 
        << "6. remove '��� ���������' - ������� ������ �� ������" << std::endl 
        << "7. search '���' - ����� �������" << std::endl 
        << "8. cls - �������� ��������� ������" << std::endl
        << "9. author - �� ������" << std::endl
        << "10. exit - �����" << std::endl; 
}


void exit(const char* line) {
    std::exit(0);
}

void author(const char*) {
    setlocale(LC_ALL, "Russian");

    std::cout << "�������� ������: ";
    std::cout << "������� ������, ������� ������ �����-12." << std::endl;
    std::cout << "������ �� GitLab: https://gitlab.com/f5d5/coursework.git" << std::endl;
}

// �����: COLOR DANCE
void color_dance(const char*) {
    setlocale(LC_ALL, "Russian");
    std::cout << "�� �������� �����: COLOR DANCE!" << std::endl;
    int num;
    srand(time(NULL));
    while(true) {
        system("color 0A");
        num = rand() % 2;
        std::cout << num;
        system("color 0B");
        num = rand() % 2;
        std::cout << num; 
        system("color 0C");
        num = rand() % 2;
        std::cout << num;
        system("color 0D");
        num = rand() % 2;
        std::cout << num;
        system("color 0E");
        num = rand() % 2;
        std::cout << num;
        system("color 0F");
        num = rand() % 2;
        std::cout << num;
    }
}

void hacker(const char*) {
    setlocale(LC_ALL, "Russian");
    system("color 0A");
    std::cout << "�����������, ������ �� �����!" << std::endl;
}

void load(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "ERROR: ����� ��� �����!" << std::endl;
        return;
    }
    std::ifstream fin(line);
    if (!fin.good()) {
        std::cout << "ERROR: �� ������� �������`" << line << "`" << std::endl;
        return;
    }
    fin.close();

    list.load_from(line);
}


void save(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "ERROR: ����� ��� �����!" << std::endl;
        return;
    }

    list.save_all(line);
}


void print(const char* line) {
    setlocale(LC_ALL, "Russian");
    list.print_all();
}


void remove_object(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "ERROR: ����� ���!" << std::endl;
        return;
    }

    if (std::strcmp(line, "*") == 0) {
        list.remove_all();
    }
    else {
        list.remove(line);
    }
}


void search(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "ERROR: ����� ���!" << std::endl;
        return;
    }

    Pet* pet = list.search(line);
    if (pet == nullptr) {
        std::cout << "ERROR: �� �������!" << std::endl;
    }
    else {
        std::cout << *pet;
    }
}


void add(const char* line) {
    setlocale(LC_ALL, "Russian");
    Pet* pet;

    char classname[STRING_SIZE] = {};
    char name[STRING_SIZE] = {};
    float weight;
    int height;

    std::cout << "������� �������� ������ (Cat, Dog, Cow, Horse): ";
    std::cin.getline(classname, STRING_SIZE);

    if (std::strcmp(classname, "Cat") == 0 || std::strcmp(classname, "Dog") == 0) {
        char sound[STRING_SIZE] = {}; // Predator << ����
        char host[STRING_SIZE] = {}; // Dog << ��� �������
        char color[STRING_SIZE] = {}; // Cat << ����

        bool night_vision; // Predator << ������ ������

        float distance; // Dog << ���������� ������� ������ �� ����
        float tail_length; // Cat << ����� ������
        
        int num_paws; // Predator << ���������� ���
        int num_commands; // Dog << ���������� �������� ������
        int num_mustache; // Cat << ���������� ����

        std::cout << "���: ";
        std::cin.getline(name, STRING_SIZE);
        std::cout << "���: ";
        std::cin >> weight;
        std::cout << "����: ";
        std::cin >> height;

        std::cout << "�����: ";
        linebreak();
        std::cin.getline(sound, STRING_SIZE);
        std::cout << "���� �� ������ ������: ";
        std::cin >> night_vision;
        std::cout << "���������� ���: ";
        std::cin >> num_paws;

        if (std::strcmp(classname, "Cat") == 0) {
            std::cout << "����: ";
            linebreak();
            std::cin.getline(color, STRING_SIZE);
            std::cout << "����� ������: ";
            std::cin >> tail_length;
            std::cout << "���������� ����: ";
            std::cin >> num_mustache;
            linebreak();

            pet = new Cat(name, weight, height, sound, night_vision, num_paws, color, tail_length, num_mustache);
        }
        else {
            std::cout << "��� �������: ";
            linebreak();
            std::cin.getline(host, STRING_SIZE);
            std::cout << "���������� ����������: ";
            std::cin >> distance;
            std::cout << "���������� ��������� ������: ";
            std::cin >> num_commands;
            linebreak();
            
            pet = new Dog(name, weight, height, sound, night_vision, num_paws, host,  distance, num_commands);
        }
    }
    else if (std::strcmp(classname, "Cow") == 0 || std::strcmp(classname, "Horse") == 0) {
        char stat[STRING_SIZE] = {}; // Herbivorous << ������ ��������� (����� -- ��������)

        bool home; // Herbivorous << ���� � ����

        float milk; // Cow << ���������� ������, ������� ���� ������.
        float speed; // Horse << �������� ������

        int num_eyes; // Herbivorous << ���� ���� � ���������

        std::cout << "���: ";
        std::cin.getline(name, STRING_SIZE);
        std::cout << "���: ";
        std::cin >> weight;
        std::cout << "����: ";
        std::cin >> height;

        std::cout << "����� (Dead or Alive): ";
        std::cin >> stat;
        std::cout << "���� � ����: ";
        std::cin >> home;
        std::cout << "���������� ����: ";
        std::cin >> num_eyes;

        if (std::strcmp(classname, "Cow") == 0) {
            std::cout << "���������� ������ ������: ";
            std::cin >> milk;
            pet = new Cow(name, weight, height, stat, home, num_eyes, milk);
        }
        else {
            std::cout << "������������ ��������: ";
            std::cin >> speed;
            pet = new Horse(name, weight, height, stat, home, num_eyes, speed);
        }
        linebreak();
    }
    else {
        std::cout << "����������� �����!" << std::endl;
    }
    list.append(pet);
}


void start_menu() {
    setlocale(LC_ALL, "Russian");
    CmdMap commands;
    commands["menu"] = menu;
    commands["exit"] = exit;
    commands["print"] = print;
    commands["load"] = load;
    commands["save"] = save;
    commands["remove"] = remove_object;
    commands["search"] = search;
    commands["hacker"] = hacker;
    commands["colordance"] = color_dance;
    commands["add"] = add;
    commands["author"] = author;
    std::cout << "����� ������ ��������� ������� �������: menu." << std::endl; 

    while (true) {
        char prompt[STRING_SIZE] = {};
        std::cout << "����: ";
        std::cin.getline(prompt, STRING_SIZE);

        if (std::cin.fail()) {
            std::cout << "ERROR!";
            break;
        }

        if (std::strlen(prompt) == 0) {
            continue;
        }
        
        char prompt_copy[STRING_SIZE];
        std::strcpy(prompt_copy, prompt);
        const char* command = std::strtok(prompt_copy, " \n");
        const char* line = prompt_copy + std::strlen(command) + 1;

        if (commands.find(command) == commands.end()) {
            std::system(prompt);
            continue;
        }

        commands[command](line);
    }
}
