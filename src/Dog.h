#pragma once


#include "Predator.h"
#include "config.h"

class Dog : public Predator
{
protected:
    char host_name[STRING_SIZE];
    float distance_traveled;
    int number_of_command; // ���������� ��������� ������
public:
        Dog(char*, float, int, char*, bool, int, char*, float, int);
        Dog();
        void print(std::ostream&) const;
        void save(std::ofstream&);
       void load(std::ifstream&);

};
