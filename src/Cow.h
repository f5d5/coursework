#pragma once


#include "Herbivorous.h"
#include "config.h"

class Cow : public Herbivorous
{
protected:
    float value_milk; 
public:
    Cow(char*, float, int, char*, bool, int, float);
    Cow();
    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

};
