#pragma once

#include <iostream>

#include "Pet.h"
#include "config.h"

class Predator : public Pet
{
protected:
    char sound[STRING_SIZE];
    bool night_vision;
    int number_of_paws;
public:
    Predator(char*, float, int, char*, bool, int);
    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

}; 