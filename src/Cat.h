#pragma once

#include "Predator.h"
#include "config.h"

class Cat : public Predator
{
protected:
    char color[STRING_SIZE];
    float tail_length; // ����� ������
    int number_of_mustache; // ���������� ����
public:
    Cat(char*, float, int, char*, bool, int, char*, float, int);
    Cat();
    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);
};
