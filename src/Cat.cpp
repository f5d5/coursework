#include <iostream>
#include <fstream>
#include "Cat.h"

Cat::Cat(
    char* name,
    float w,
    int h,
    char* message,
    bool nv,
    int num_paws,
    char* col, // ����
    float length_tail, // ����� ������
    int num_mustache // ���������� ����
) : Predator(name, w, h, message, nv, num_paws), tail_length(length_tail), number_of_mustache(num_mustache)
{
    classname = CAT;
    std::strcpy(color, col);
}

Cat::Cat() : Predator("����", 0.56, 2134, "���", true, 4), tail_length(2.14), number_of_mustache(1)
{
    std::strcpy(color, "White");
}

void Cat::print(std::ostream& ostream) const
{
    Predator::print(ostream);
    ostream << "����: " << color << std::endl;
    ostream << "����� ������: " << tail_length << std::endl;
    ostream << "���������� ����: " << number_of_mustache << std::endl;
}

void Cat::save(std::ofstream& fout) {
    Predator::save(fout);
    fout.write(reinterpret_cast<char*>(color), sizeof(color));
    fout.write(reinterpret_cast<char*>(&tail_length), sizeof(tail_length));
    fout.write(reinterpret_cast<char*>(&number_of_mustache), sizeof(number_of_mustache));
}


void Cat::load(std::ifstream& fin) {
    Predator::load(fin);
    fin.read(reinterpret_cast<char*>(color), sizeof(color));
    fin.read(reinterpret_cast<char*>(&tail_length), sizeof(tail_length));
    fin.read(reinterpret_cast<char*>(&number_of_mustache), sizeof(number_of_mustache));
}
