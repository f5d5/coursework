#pragma once

const size_t STRING_SIZE = 100;
enum ClassName {
        PET,
        PREDATOR,
        HERBIVOROUS,
        CAT,
        DOG,
        COW,
        HORSE
};