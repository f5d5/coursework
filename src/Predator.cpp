#pragma once
#include <fstream>
#include "Predator.h"

Predator::Predator(char* name, float w, int h, char* message, bool nv, int num_paws) : Pet(name, w, h), night_vision(nv), number_of_paws(num_paws)
{
    classname = PREDATOR;
    std::strcpy(sound, message);
}
void Predator::print(std::ostream& ostream) const
{
    Pet::print(ostream);
    ostream << "����: " << sound << std::endl;
    ostream << "���� �� ������ ������: " << night_vision << std::endl;
    ostream << "���������� ���: " << number_of_paws << std::endl;
}

void Predator::save(std::ofstream& fout) {
    Pet::save(fout);
    fout.write(reinterpret_cast<char*>(sound), sizeof(sound));
    fout.write(reinterpret_cast<char*>(&night_vision), sizeof(night_vision));
    fout.write(reinterpret_cast<char*>(&number_of_paws), sizeof(number_of_paws));
}


void Predator::load(std::ifstream& fin) {
    Pet::load(fin);
    fin.read(reinterpret_cast<char*>(sound), sizeof(sound));
    fin.read(reinterpret_cast<char*>(&night_vision), sizeof(night_vision));
    fin.read(reinterpret_cast<char*>(&number_of_paws), sizeof(number_of_paws));
}
