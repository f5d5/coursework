#include <fstream>

#include "Cow.h"

Cow::Cow(char* name, float w, int h, char* stat, bool home, int num_eyes, float milk) : Herbivorous(name, w, h, stat, home, num_eyes), value_milk(milk)
{
    classname = COW;
}

Cow::Cow() : Herbivorous("lll", 123, 1, "c++ suka", false, 7575), value_milk(282)
{
    classname = COW;
}

void Cow::print(std::ostream& ostream) const
{
    Herbivorous::print(ostream);
    ostream << "������� ������: " << value_milk << std::endl;
}

void Cow::save(std::ofstream& fout) {
    Herbivorous::save(fout);
    fout.write(reinterpret_cast<char*>(&value_milk), sizeof(value_milk));
}


void Cow::load(std::ifstream& fin) {
    Herbivorous::load(fin);
    fin.read(reinterpret_cast<char*>(&value_milk), sizeof(value_milk));
}