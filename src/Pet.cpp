#include <cstring>
#include <fstream>
#include <iostream>

#include "Pet.h"

Pet::Pet()
{

}

Pet::Pet(char* name, float w, int h) : height(h), weight(w)
{
    std::strcpy(animal_name, name);
}

int Pet::getHeight() {
    return height;
}

float Pet::getWeight() {
    return weight;
}

char* Pet::getName() {
    return animal_name;
}
void Pet::print(std::ostream& ostream) const
{
    ostream << "���: " << animal_name << std::endl;
    ostream << "���: " << weight << std::endl;
    ostream << "������: " << height << std::endl;
}

void Pet::save(std::ofstream& fout) { 
    fout.write(reinterpret_cast<char*>(&classname), sizeof(classname));
    fout.write(reinterpret_cast<char*>(animal_name), sizeof(animal_name));
    fout.write(reinterpret_cast<char*>(&weight), sizeof(weight));
    fout.write(reinterpret_cast<char*>(&height), sizeof(height));
}


void Pet::load(std::ifstream& fin) {
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.read(reinterpret_cast<char*>(animal_name), sizeof(animal_name));
    fin.read(reinterpret_cast<char*>(&weight), sizeof(weight));
    fin.read(reinterpret_cast<char*>(&height), sizeof(height));
}

std::ostream& operator<<(std::ostream& ostream, const Pet& pet) { // ���������� 
    pet.print(ostream);
    return ostream;
}


