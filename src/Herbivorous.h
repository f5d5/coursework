#pragma once

#include <iostream>
#include "Pet.h"
#include "config.h"


class Herbivorous : public Pet
{
protected:
    char status[STRING_SIZE];
    bool location_home = true;
    int number_of_eyes = 2;
public:
    Herbivorous(char*, float, int, char*, bool, int);
    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

};
