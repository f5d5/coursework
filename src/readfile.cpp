#include <fstream>
#include <iostream>
#include <cstring>
#include <limits>

#include "readfile.h"
#include "config.h"
#include "Cat.h"
#include "Dog.h"
#include "Cow.h"
#include "Horse.h"


Pet* read_from_file_smart(const char* filename, size_t& offset) {

    ClassName classname;
    std::ifstream fin(filename, std::ios::binary);
    fin.seekg(offset);
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.seekg(offset);

    Pet* pet;
    switch (classname) {
    case CAT:
        pet = new Cat;
        break;
    case DOG:
        pet = new Dog;
        break;
    case COW:
        pet = new Cow;
        break;
    case HORSE:
        pet = new Horse;
        break;
    default:
        return nullptr;
    }

    pet->load(fin);

    if (fin.fail()) {
        return nullptr;
    }

    offset = fin.tellg();
    fin.close();

    return pet;
}


void parse_words(const char string[], char words[][STRING_SIZE]) {
    static char sep[] = " \n\t\r";

    size_t string_length = std::strlen(string);
    char* string_copy = new char[string_length + 4]();
    std::strcpy(string_copy, string);

    char* item = std::strtok(string_copy, sep);
    for (size_t i = 0; item; i++) {
        std::strcpy(words[i], item);
        item = std::strtok(NULL, sep);
    }

    delete[] string_copy;
}

void linebreak() {
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
