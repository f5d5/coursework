#include <fstream>

#include "Horse.h"

Horse::Horse(char* name, float w, int h, char* stat, bool home, int num_eyes, float sp) : Herbivorous(name, w, h, stat, home, num_eyes), speed(sp)
{
    classname = HORSE;
    
}
Horse::Horse() : Herbivorous("qwer", 5.555, 1, "VIVIVIVVIVI", true, 666), speed(22)
{
    classname = HORSE;
}
void Horse::print(std::ostream& ostream) const
{
    Herbivorous::print(ostream);
    ostream << "����� ��������: " << speed << std::endl;
}

void Horse::save(std::ofstream& fout) {
    Herbivorous::save(fout);
    fout.write(reinterpret_cast<char*>(&speed), sizeof(speed));
}


void Horse::load(std::ifstream& fin) {
    Herbivorous::load(fin);
    fin.read(reinterpret_cast<char*>(&speed), sizeof(speed));
}